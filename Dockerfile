FROM node:16
WORKDIR /offshorevn-fe
COPY package.json .
COPY .npmrc .
RUN npm config set registry https://registry.npmjs.org/
RUN npm install
COPY . .
RUN npm run build
CMD ["npm", "start"]
